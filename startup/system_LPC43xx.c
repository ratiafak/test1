/**
 * @file system_lpc43xx.c
 * @brief Cortex-M4 Initialization code.
 */

#include "LPC43xx.h"


uint32_t SystemCoreClock = 12000000;	///< Core speed (running from IRC)

/**
 * @brief  Initialize the core and set basic clocks.
 */
void SystemInit(void)
{
	volatile uint32_t delay;

// Set up Cortex_M3 or M4 VTOR register to point to vector table
// This code uses a toolchain defined symbol to locate the vector table
// If this is not completed, interrupts are likely to cause an exception.
	unsigned int * pSCB_VTOR = (unsigned int *)0xE000ED08;

	extern void *g_pfnVectors;

	// CodeRed - correct to assign address of variable not contents
	*pSCB_VTOR = (unsigned int)&g_pfnVectors;

// LPC18xx/LPC43xx ROM sets the PLL to run from IRC and drive the part
// at 96 MHz out of reset
	SystemCoreClock = 96000000;

// Speed up to full speed
	//1. Select the IRC as BASE_M4_CLK source.
	LPC_CGU->BASE_M4_CLK = (LPC_CGU->BASE_M4_CLK & ~(0x1f << 24)) | (0x01 << 24);

	//2. Enable the crystal oscillator (see Table 126).
	LPC_CGU->XTAL_OSC_CTRL &= ~0x6;	//Not bypass, low frequency
	LPC_CGU->XTAL_OSC_CTRL &= ~0x1;	//Enable

	//3. Wait 250 us.
	for(delay = 250*12; delay; delay--);	//250 ticks at 12 MHz (calibrate length of for loop?)

	//4. Set the AUTOBLOCK bit (bit 11). This bit re-synchronizes the clock output during
	//frequency changes that prevents glitches when switching clock frequencies.
	LPC_CGU->BASE_M4_CLK |= (1 << 11);

	//5. Reconfigure PLL1 as follows (see Table 137):
	//– Select the M and N divider values to produce the final desired PLL1 output frequency f outPLL.
	//– Select the crystal oscillator as clock source for PLL1.
	LPC_CGU->PLL1_CTRL |= (0x1 << 0);	//Power off PLL
	LPC_CGU->PLL1_CTRL &= ~((0x1 << 1) | (0x1 << 6)); //No bypass, feedback from CCO
	LPC_CGU->PLL1_CTRL |= (0x1 << 11);	//Enable autoblock
	LPC_CGU->PLL1_CTRL = (LPC_CGU->PLL1_CTRL & ~(0x3 << 12)) | (0x0 << 12);	//N=1
	LPC_CGU->PLL1_CTRL = (LPC_CGU->PLL1_CTRL & ~(0xff << 16)) | ((17-1) << 16);	//M=17 (12 MHz * 17 = 204 MHz)
	LPC_CGU->PLL1_CTRL = (LPC_CGU->PLL1_CTRL & ~(0xf << 24)) | (0x6 << 24);	//Crystal oscillator
	LPC_CGU->PLL1_CTRL &= ~(0x1 << 0);	//Power on PLL

	//6. Wait for the PLL1 to lock.
	while(!(LPC_CGU->PLL1_STAT & 0x1));

	//7. Set the PLL1 P-divider to divide by 2 (DIRECT = 0, PSEL=0).
	LPC_CGU->PLL1_CTRL &= ~((0x1 << 7) | (0x3 << 8));	//Not direct, divide output by 2x1

	//8. Select PLL1 as BASE_M4_CLK source. The BASE_M4_CLK now operates in the mid-frequency range.
	LPC_CGU->BASE_M4_CLK = (LPC_CGU->BASE_M4_CLK & ~(0x1f << 24)) | (0x09 << 24);

	//9. Wait 50 us.
	for(delay = 50*102; delay; delay--);	//50 ticks at 102 MHz (calibrate length of for loop?)

	//10. Set the PLL1 P-divider to direct output mode (DIRECT = 1). The BASE_M4_CLK now operates in the high-frequency range.
	LPC_CGU->PLL1_CTRL |= (0x1 << 7);

	//The BASE_M4_CLK now operates in the high-frequency range.
	SystemCoreClock = 204000000;
}


